import Vue from 'vue'
import { MdField, MdMenu, MdList, MdSwitch, MdIcon, MdButton, MdRadio } from 'vue-material/dist/components'

Vue.use(MdField)
Vue.use(MdMenu)
Vue.use(MdList)
Vue.use(MdSwitch)
Vue.use(MdIcon)
Vue.use(MdButton)
Vue.use(MdRadio)
