# Ventur

## Build Setup

```bash
# install dependencies
$ yarn install

# serve with hot reload at localhost:3000
$ yarn dev

# build for production and launch server (for local setup, see below)
$ yarn build
$ yarn start

# generate static project
$ yarn generate
```

For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org).

## Local Setup

MailChimp integration has been set up for testing using: https://hashinteractive.com/blog/nuxt-js-mailchimp-integration-add-contact-to-list/

## Netlify Setup

Production MailChimp integration is set up using Netlify Functions: https://functions.netlify.com/example/progressive-mailchimp-subscription-form/

For an alternative local developement run:

```
netlify dev
```

See: https://docs.netlify.com/cli/get-started/#netlify-dev