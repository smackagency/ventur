const axios = require('axios')
const client = require('@sendgrid/mail')

exports.handler = async function (event, context, callback) {
  const {
    name,
    company,
    email,
    phone,
    queryType,
    existingCustomerType,
    message,
    recaptchaToken,
  } = JSON.parse(event.body)

  try {
    const response = await axios.get(`https://www.google.com/recaptcha/api/siteverify?secret=${process.env.RECAPTCHA_SECRET_KEY}&response=${recaptchaToken}`)

    if (response.data.success && response.data.score >= 0.7) {
      let
        to = [
          'marketing@gdg.travel',
          'sales@gdg.travel',
          'account.management@gdg.travel',
        ],
        text = `Name:\n${name}\n\n` +
               `Company:\n${company}\n\n` +
               `Email:\n${email}\n\n` +
               `Phone:\n${phone}\n\n` +
               `Query type:\n${queryType}\n\n`
      
      if (queryType == 'I’m an existing customer with a query') {
        text += `Existing customer type:\n${existingCustomerType}\n\n`
      }

      text += `Message:\n${message}`

      if (queryType == 'I’m a new customer interested in partnering with Ventur') {
        to.push('workwithus@ventur.partners')
        to.push('workwithusalerts@ventur.partners')
      }

      if (queryType == 'I’m an existing customer with a query') {
        if (existingCustomerType == 'Corporate travel') {
          to.push('book@ventur.partners')
        }
        
        if (existingCustomerType == 'Group travel' || existingCustomerType == 'Sports travel') {
          to.push('groups@ventur.partners')
        }

        if (existingCustomerType == 'Accounts') {
          to.push('accounts@ventur.partners')
        }
      }

      if (queryType == 'I have a press enquiry') {
        to.push('marketing@ventur.partners')
      }

      if (queryType == 'I want to speak to you about something else') {
        to.push('hello@ventur.partners')
      }

      client.setApiKey(process.env.SENDGRID_API_KEY)

      try {
        await client.send({
          to: to,
          from: 'notifications@ventur.partners',
          subject: `Ventur website notification: ${queryType}`,
          text: text,
        })

        return {
          statusCode: 200,
          body: 'Message sent',
        }
      } catch (err) {
        return {
          statusCode: err.code,
          body: JSON.stringify({ msg: err.message }),
        }
      }
    } else {
      return {
        statusCode: 500,
        body: JSON.stringify({ msg: 'Invalid token' }),
      }
    }
  } catch (e) {
    return {
      statusCode: 500,
      body: JSON.stringify({ msg: e }),
    }
  }
}