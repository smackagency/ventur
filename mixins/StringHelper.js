export default {
  methods: {
    nl2br (string) {
      return string ? string.replace(/(?:\r\n|\r|\n)/g, '<br>') : ''
    },
  },
};