export default {
  // Global page headers (https://go.nuxtjs.dev/config-head)
  head: {
    title: "ventur",
    meta: [
      {
        "http-equiv": "X-UA-Compatible",
        content: "IE=edge"
      },
      {
        charset: "utf-8"
      },
      {
        name: "viewport",
        content: "width=device-width, initial-scale=1"
      },
      {
        name: "msapplication-TileColor",
        content: "#00303d"
      },
      {
        name: "theme-color",
        content: "#ffffff"
      }
    ],
    link: [
      {
        rel: "apple-touch-icon",
        sizes: "180x180",
        href: "/apple-touch-icon.png"
      },
      {
        rel: "icon",
        type: "image/png",
        sizes: "32x32",
        href: "/favicon-32x32.png"
      },
      {
        rel: "icon",
        type: "image/png",
        sizes: "16x16",
        href: "/favicon-16x16.png"
      },
      {
        rel: "manifest",
        href: "/site.webmanifest"
      },
      {
        rel: "mask-icon",
        href: "/safari-pinned-tab.svg",
        color: "#00303d"
      }
    ],
    script: [
      {
        src:
          "https://cdn.polyfill.io/v2/polyfill.min.js?features=Element.prototype.classList"
      },
      {
        src:
          "https://cdn.jsdelivr.net/npm/focus-visible@5.0.2/dist/focus-visible.min.js"
      },
      {
        src: "https://static.cdn.prismic.io/prismic.js?new=true&repo=ventur"
      }
    ]
  },

  styleResources: {
    scss: ["~/assets/scss/_variables.scss", "~/assets/scss/_mixins.scss"]
  },

  // Global CSS (https://go.nuxtjs.dev/config-css)
  css: [
    "vue-material/dist/vue-material.min.css",
    "vue-material/dist/theme/default.css",
    "~/assets/scss/_reset.scss",
    "~/assets/scss/_fonts.scss",
    "~/assets/scss/_base.scss"
  ],

  // Plugins to run before rendering page (https://go.nuxtjs.dev/config-plugins)
  plugins: [
    "@/plugins/vue-material.client.js",
    "@/plugins/vue-agile",
    {
      src: "@/plugins/vee-validate.js",
      ssr: false
    }
  ],

  // Auto import components (https://go.nuxtjs.dev/config-components)
  components: true,

  // Modules for dev and build (recommended) (https://go.nuxtjs.dev/config-modules)
  buildModules: ["@nuxtjs/dotenv"],

  // Modules (https://go.nuxtjs.dev/config-modules)
  modules: [
    "@nuxtjs/style-resources",
    "nuxt-lazy-load",
    "@nuxtjs/gtm",
    [
      "@nuxtjs/recaptcha",
      {
        hideBadge: true,
        version: 3
      }
    ],
    [
      "@nuxtjs/prismic",
      {
        endpoint: "https://ventur.cdn.prismic.io/api/v2",
        apiOptions: {
          routes: [
            {
              type: "page",
              path: "/:uid"
            },
            {
              type: "case_study",
              path: "/experience/:uid"
            },
            {
              type: "blog_post",
              path: "/news/:uid"
            }
          ]
        },
        linkResolver: function(doc) {
          if (doc.isBroken) {
            return "/not-found";
          }

          if (doc.type === "blog_post") {
            return "/news/" + doc.uid;
          }

          if (doc.type === "case_study") {
            return "/experience/" + doc.uid;
          }

          if (
            doc.type === "insights" ||
            doc.type === "faq" ||
            doc.type === "legal" ||
            doc.type === "page"
          ) {
            return "/" + doc.uid;
          }

          return "/not-found";
        }
      }
    ],
    ["nuxt-sm"]
  ],

  // Build Configuration (https://go.nuxtjs.dev/config-build)
  build: {
    transpile: [
      "vue-slicezone",
      "nuxt-sm",
      "vee-validate/dist/rules",
      "vue-agile"
    ],
    postcss: {
      preset: {
        autoprefixer: {
          grid: true
        }
      }
    },
    friendlyErrors: false
  },

  serverMiddleware: [
    {
      path: "/api",
      handler: "~/api/index.js"
    }
  ],

  target: "static",

  env: {},

  gtm: {
    id: "GTM-M8LP7V6"
  },

  generate: {
    fallback: "404.html" // Netlify reads a 404.html, Nuxt will load as an SPA
  },

  hooks: {
    "generate:page": page => {
      page.html = page.html
        .replace(/ data-n-head=".*?"/gi, "")
        .replace(/ data-hid=".*?"/gi, "");
    }
  },

  publicRuntimeConfig: {
    recaptcha: {
      siteKey: process.env.RECAPTCHA_SITE_KEY
    },
    env: {
      isAnonymised: process.env.IS_ANONYMISED
    }
  }
};
