export const state = () => ({
  headerMenu: {}
})

export const mutations = {
  SET_HEADER_MENU(state, headerMenu) {
    state.headerMenu = headerMenu
  }
}

export const actions = {
  async fetchHeaderMenu({ commit }, $prismic) {
    const headerMenu = (await $prismic.api.getSingle('header_menu')).data
    commit('SET_HEADER_MENU', headerMenu)
  }
}
